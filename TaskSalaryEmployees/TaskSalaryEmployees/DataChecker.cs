﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TaskSalaryEmployees

{
    public class DataChecker
    {
        public static void CheckCountLeaders(List<Employee> employees, List<string> departaments)
        {
            for (int i = 0; i < departaments.Count; i++)
            {
                if (employees.Where(x => x.Departaments == departaments[i] && x.PersonIsLeader == true).ToList().Count > 2)
                {
                    throw new Exception("Неккоректные данные, количество руководителей в одном цеху превышает допустимое");
                }
            }
        }
    }
}
