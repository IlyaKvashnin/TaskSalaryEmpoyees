﻿using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;


namespace TaskSalaryEmployees
{
    public class DataWriter
    {
        public static void GenerateJsonFile()
        {
            string jsonString = JsonConvert.SerializeObject(GeneratorData.GenerateListEmployee());
            File.WriteAllText(GeneratorData.PathOfFile, jsonString);
        }
    }
}
