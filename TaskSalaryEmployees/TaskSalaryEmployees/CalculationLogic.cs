﻿using System.Collections.Generic;
using System.Linq;

namespace TaskSalaryEmployees
{
    public class CalculationLogic
    {
        public static List<string> FiltredDepartaments = DataReader.readedFile.Select(x => x.Departaments).Distinct().ToList();

        public static int CalculateAverageSalary(List<Employee> employees, string departament)
        {
            var filtredEmployees = employees.Where(x => x.Departaments == departament && x.PersonIsLeader == false).ToList();

            int averageSalary = 0;

            foreach (var employee in filtredEmployees)
            {
                averageSalary += employee.Salary;
            }

            return averageSalary / filtredEmployees.Count();
        }


        public static int FindMaxSalary(List<Employee> employees)
        {
            var filtredLeaders = employees.Where(x => x.PersonIsLeader);

            var filtredSalary = filtredLeaders.Select(x => x.Salary).ToList();

            int maxSalary = 0;

            for (int i = 0; i < filtredSalary.Count(); i++)
            {
                if (filtredSalary[i] > maxSalary)
                {
                    maxSalary = filtredSalary[i];
                }
            }

            return maxSalary;

        }
        public static string FindDepartamentWithHighestSalary(List<Employee> employees, int maxSalary)
        {
            var filtredEmployees = employees.Where(x => x.Salary == maxSalary).ToList();

            string departament = string.Empty;

            foreach (var employee in filtredEmployees)
            {
                departament = employee.Departaments;
            }

            return departament;
        }
    }
}
