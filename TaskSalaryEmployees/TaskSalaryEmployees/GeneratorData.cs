﻿using System;
using System.Collections.Generic;

namespace TaskSalaryEmployees
{
    public class GeneratorData
    {
        public const string PathOfFile = "../../../../../DataOfEmployees.json";

        public static List<Employee> GenerateListEmployee()
        {
            return new List<Employee>()
            {
                new Employee { FullName = "Иванов Иван Иванович", Departaments = "Цех 1", Salary = 25000},
                new Employee { FullName = "Иванов Петр Георгиевич", Departaments = "Цех 1", Salary = 22000},
                new Employee { FullName = "Петров Игорь Васильевич", Departaments = "Цех 3", Salary = 35000, PersonIsLeader = true },
                new Employee { FullName = "Горбунов Лев Рудольфович", Departaments = "Цех 1", Salary = 40000, PersonIsLeader = true},
                new Employee { FullName = "Кудряшов Егор Ростиславович", Departaments = "Цех 2", Salary = 20000},
                new Employee { FullName = "Миронов Ростислав Тихонович", Departaments = "Цех 3", Salary = 20000},
                new Employee { FullName = "Зиновьев Павел Филиппович", Departaments = "Цех 3", Salary = 20000, PersonIsLeader = true},
                new Employee { FullName = "Карпов Ян Викторович", Departaments = "Цех 3", Salary = 20000},
                new Employee { FullName = "Иванков Михаил Данилович", Departaments = "Цех 3", Salary = 20000},
                new Employee { FullName = "Елисеев Наум Улебович", Departaments = "Цех 2", Salary = 60000, PersonIsLeader = true}
            };
        }
    }
}
