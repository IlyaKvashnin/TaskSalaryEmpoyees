﻿using System;
using System.Collections.Generic;

namespace TaskSalaryEmployees
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> readedEmployee = DataReader.ReadFile(GeneratorData.PathOfFile);

            FileChecker.CheckFile(GeneratorData.PathOfFile);

            DataChecker.CheckCountLeaders(DataReader.readedFile, CalculationLogic.FiltredDepartaments);

            OutputLogic.PrintAverageSalary(CalculationLogic.FiltredDepartaments);

            Console.WriteLine("\n" + "Отдел с самым высокооплачиваемым сотрудником: {0} ", CalculationLogic.FindDepartamentWithHighestSalary(DataReader.readedFile, CalculationLogic.FindMaxSalary(DataReader.readedFile)));
        }
    }
}

//Калькулятор конструктор с датаридером
//Непрозрачно 
//Считали, сохранили, 