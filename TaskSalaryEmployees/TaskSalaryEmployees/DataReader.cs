﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace TaskSalaryEmployees
{
    public class DataReader
    {
        public static List<Employee> readedFile = ReadFile(GeneratorData.PathOfFile);

        public static List<Employee> ReadFile(string path)
        {
            string readedFile = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<List<Employee>>(readedFile);
        }
    }
}
