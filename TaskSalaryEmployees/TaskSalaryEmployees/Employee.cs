﻿using System;
using System.Collections.Generic;

namespace TaskSalaryEmployees
{
    public class Employee
    {
        public string FullName { get; set; }
        public string Departaments { get; set; }
        public int Salary { get; set; }
        public bool PersonIsLeader { get; set;  }
    }
}
